﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace JobInterface.Database
{
    public class TableAdapter :  IDatabaseObject
    {
        private DatabaseConnection dbConnection;
        private string _table;
        private string _sql = null;
        private StoredProcedure _procedure = null;
        private SqlConnection sqlConnection;
        private SqlDataAdapter dataAdapter;
        private SqlCommandBuilder command;
        private DataTable _dataTable;

        public DataTable DataTable
        {
            get { return _dataTable; }
        }

        public TableAdapter(DatabaseConnection dbConnection)
        {
            this.dbConnection = dbConnection;
        }

        public TableAdapter(DatabaseConnection dbConnection, string table)
        {
            this._table = table;
        }

        public TableAdapter(DatabaseConnection dbConnection, string table, string sql)
            : this(dbConnection, table)
        {
            this._sql = sql;
            //_dataAdapter = new SqlDataAdapter(sql, _sqlConnection);
        }

        public TableAdapter(DatabaseConnection dbConnection, string table, StoredProcedure procedure)
            : this(dbConnection, table)
        {
            this._procedure = procedure;
            //_dataAdapter = new SqlDataAdapter(procedure.SqlCommand, _sqlConnection);
        }

        public DataTable GetTable()
        {
            try
            {
                if (sqlConnection == null)
                    sqlConnection = new SqlConnection(dbConnection.ConnectionString);


                if (_procedure != null)
                {
                    _procedure.SqlCommand.Connection = sqlConnection;
                    dataAdapter = new SqlDataAdapter(_procedure.SqlCommand);
                }
                else if (_sql != null)
                    dataAdapter = new SqlDataAdapter(_sql, sqlConnection);
                else
                    dataAdapter = new SqlDataAdapter("SELECT * FROM " + _table, sqlConnection);
                
                command = new SqlCommandBuilder(dataAdapter);
                DataSet dataSet = new DataSet();
                dataSet.EnforceConstraints = false;
                dataAdapter.Fill(dataSet, _table);
                _dataTable = dataSet.Tables[_table];
                                
                //_dataAdapter.Dispose();

                return _dataTable;            
            }
            catch (Exception exception)
            {
                if (sqlConnection != null)
                    sqlConnection.Close();                

                //this.exception = exception;

                if (this.dbConnection.ErrorHandling != null)
                    this.dbConnection.ErrorHandling(this, exception);

                return new DataTable("Error");
            }
        }

        public int Update(DataTable dataTable)
        {
            int result;

            try
            {
                //SqlDataAdapter dataAdapter = new SqlDataAdapter("SELECT * FROM " + _table, sqlConnection);
                //SqlCommandBuilder command = new SqlCommandBuilder(dataAdapter);
                //DataSet dataSet = new DataSet();
                //_dataAdapter.Fill(dataSet, _table);

                result = dataAdapter.Update(dataTable);
                dataTable.AcceptChanges();
                
                //command.Dispose();
                //dataAdapter.Dispose();
            }
            catch (Exception exception)
            {
                //_sqlConnection.Close();

                //this.exception = exception;

                if (this.dbConnection.ErrorHandling != null)
                    this.dbConnection.ErrorHandling(this, exception);

                result = 0;
            }
            finally
            {
                dataTable.AcceptChanges();               
            }

            return result;
        }

        public int Update(DataRow[] dataRows)
        {
            try
            {
                return dataAdapter.Update(dataRows);
            }
            catch (Exception exception)
            {
                //this.exception = exception;

                if (this.dbConnection.ErrorHandling != null)
                    this.dbConnection.ErrorHandling(this, exception);

                return 0;
            }
        }

        public int Insert(DataTable dataTable)
        {
            if (dataTable == null)
                return 0;

            if(this.dataAdapter == null)
                this.GetTable();
           
            foreach (DataRow row in dataTable.Rows)
                row.SetAdded();

            return this.dataAdapter.Update(dataTable);
        }

        public void Close()
        {
            try
            {
                if(this.command != null)
                    this.command.Dispose();
                if (this.dataAdapter != null)
                    this.dataAdapter.Dispose();
                if (this.sqlConnection != null)
                {
                    this.sqlConnection.Close();
                    this.sqlConnection.Dispose();
                }
            }
            catch (Exception exception)
            {
                //this.exception = exception;

/*                if (DatabaseConnection.ErrorHandling != null)
                    DatabaseConnection.ErrorHandling(this, exception);*/
                if (this.dbConnection.ErrorHandling != null)
                    this.dbConnection.ErrorHandling(this, new Exception("TableAdapter: " + this._table, exception));
            }
        }

        ~TableAdapter()
        {
            this.Close();
        }
    }
}

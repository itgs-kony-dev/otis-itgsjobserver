﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace JobInterface.Database
{
    public class StoredProcedure : IDatabaseObject
    {
        private DatabaseConnection dbConnection;
        private string _procedure;
        SqlParameterCollection _parameters;
        SqlCommand _command;

        public SqlParameterCollection Parameters
        {
            get { return _command.Parameters; }
        }

        public SqlCommand SqlCommand
        {
            get { return _command; }
        }

        public StoredProcedure(DatabaseConnection dbConnection)
        {
            this.dbConnection = dbConnection;

            _parameters = new SqlCommand().Parameters;
            
            if(_command == null)
                _command = new SqlCommand();

            _command.CommandType = CommandType.StoredProcedure;
        }

        public StoredProcedure(DatabaseConnection dbConnection, string procedure)
        {
            this.dbConnection = dbConnection;

            this._procedure = procedure;
            //_parameters = new SqlCommand().Parameters;
            _command = new SqlCommand(procedure);
            _command.CommandType = CommandType.StoredProcedure;

        }

        public object ExecuteScalar()
        {
            using (SqlConnection sqlConnection = new SqlConnection(dbConnection.ConnectionString))
            {
                try
                {
                    sqlConnection.Open();
                    //SqlCommand _command = new SqlCommand(_procedure, sqlConnection);
                    _command.Connection = sqlConnection;
                    //foreach (SqlParameter parameter in _parameters)
                    //    _command.Parameters.Add(parameter);

                    object value = _command.ExecuteScalar();

                    _command.Dispose();

                    return value;
                }
                catch (Exception exception)
                {
                    //this.exception = exception;
                    this.ErrorHandling(exception);

                    return "Error";
                }
                finally
                {
                    sqlConnection.Close();
                }
            }
        }

        public DataTable GetTable()
        {
            using (SqlConnection sqlConnection = new SqlConnection(dbConnection.ConnectionString))
            {
                try
                {
                    sqlConnection.Open();
                    //SqlCommand _command = new SqlCommand(_procedure, sqlConnection);
                    _command.Connection = sqlConnection;
                    _command.CommandType = CommandType.StoredProcedure;
                    //foreach (SqlParameter parameter in _parameters)
                    //    _command.Parameters.Add(parameter);
                    
                    SqlDataReader reader = _command.ExecuteReader();

                    DataTable dataTable = new DataTable();
                   
                    using (DataSet ds = new DataSet() { EnforceConstraints = false })
                    {
                        ds.Tables.Add(dataTable);
                        dataTable.Load(reader, LoadOption.OverwriteChanges);
                        ds.Tables.Remove(dataTable);
                    }
                    //dataTable.Load(reader);

                    reader.Close();
                    reader.Dispose();
                    _command.Dispose();

                    return dataTable;
                }
                catch (Exception exception)
                {
                    //this.exception = exception;
                    this.ErrorHandling(exception);

                    return new DataTable("Error");
                }
                finally
                {
                    sqlConnection.Close();
                }
            }
        }

        private new void ErrorHandling(Exception exception)
        {
            if (this.dbConnection.ErrorHandling != null)
                this.dbConnection.ErrorHandling(this, new Exception("StoredProcedure-Error: " + this._procedure, exception));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace JobInterface.Database
{
    public delegate void OnError(object sender, Exception exception);

    public class DatabaseConnection
    {
        private string _connectionString;
        public event OnError OnError;
        protected Exception exception = null;

        public string ConnectionString
        {
            get { return _connectionString; }
            set { _connectionString = value; }
        }

        public OnError ErrorHandling
        {
            get { return OnError; }
            protected set { OnError = value; }
        }

        public Exception Exception
        {
            get { return this.exception; }
        }

        public bool isError
        {
            get { return (object.ReferenceEquals(exception, null) == false); }
        }

        /*public DatabaseConnection()
        {
        }*/

        public DatabaseConnection(string connectionString, OnError onError)
        {
            _connectionString = connectionString;
            OnError = onError;
        }

        public SqlConnection SqlConnection
        {
            get
            {
                return new SqlConnection(ConnectionString);
            }
        }
    }
}

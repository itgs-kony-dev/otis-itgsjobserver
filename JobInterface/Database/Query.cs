﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace JobInterface.Database
{
    public class Query : IDatabaseObject
    {
        private DatabaseConnection dbConnection;
        private string _query;

        public Query(DatabaseConnection dbConnection)
        {
            this.dbConnection = dbConnection;
        }

        public Query(DatabaseConnection dbConnection, string query)
            :this(dbConnection)
        {
            this._query = query;
        }

        public object ExecuteScalar()
        {
            using (SqlConnection sqlConnection = new SqlConnection(dbConnection.ConnectionString))
            {
                try
                {
                    sqlConnection.Open();
                    SqlCommand command = new SqlCommand(_query, sqlConnection);

                    object value = command.ExecuteScalar();

                    command.Dispose();

                    return value;
                }
                catch (Exception exception)
                {
                    //this.dbConnection.exception = exception;

                    /*if (DatabaseConnection.ErrorHandling != null)
                        DatabaseConnection.ErrorHandling(this, exception);*/
                    if (this.dbConnection.ErrorHandling != null)
                        this.dbConnection.ErrorHandling(this, new Exception("SQL-Error: " + this._query, exception));

                    return "Error";
                }
                finally
                {
                    sqlConnection.Close();
                }
            }
        }

        public DataTable GetTable()
        {
            using (SqlConnection sqlConnection = new SqlConnection(this.dbConnection.ConnectionString))
            {                
                try
                {                    
                    sqlConnection.Open();
                    SqlCommand command = new SqlCommand(_query, sqlConnection);
                    SqlDataReader reader = command.ExecuteReader();

                    DataTable dataTable = new DataTable();
                    //ConstraintCollection cc = dataTable.Constraints;

                    using (DataSet ds = new DataSet() { EnforceConstraints = false })
                    {
                        ds.Tables.Add(dataTable);
                        dataTable.Load(reader, LoadOption.OverwriteChanges);
                        ds.Tables.Remove(dataTable);
                    }
                    //dataTable.Load(reader);

                    reader.Close();
                    reader.Dispose();
                    command.Dispose();

                    return dataTable;
                }
                catch (Exception exception)
                {
                    //this.exception = exception;

                    if (this.dbConnection.ErrorHandling != null)
                        this.dbConnection.ErrorHandling(this, exception);

                    return new DataTable("Error");
                }
                finally
                {
                    sqlConnection.Close();
                }
            }
        }
    }
}

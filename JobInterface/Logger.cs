﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace JobInterface
{
    public class Logger
    {
        public delegate void LogHandler(String message, EventLogEntryType type);

        public event Logger.LogHandler LogEvent;

        public void log(String message, EventLogEntryType type = EventLogEntryType.Information)
        {
            if (this.LogEvent != null)
                this.LogEvent(message, type);
        }
    }
}

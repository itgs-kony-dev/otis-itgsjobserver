﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JobInterface
{
    public interface IJobItem
    {
        Logger logger { get; set; }

        void start();
        void stop();
        void resume();

       
    }
}

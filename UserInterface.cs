﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using System.ServiceProcess;
using System.Reflection;
using Microsoft.Win32;
using JobInterface;
using JobInterface.Database;

namespace ITGSJobServer
{
    public partial class UserInterface : Form
    {
        #if (DEBUG)
            private bool RunningState { get; set; }
            private List<JobControl> jobControl = new List<JobControl>();
            private Logger logger;
        #else        
            String serviceName = "ITGS Job Service";
            ServiceController sc;
            delegate void ActionHandler();
            event ActionHandler Action;
            int lastLogIndex = 0;
        #endif

        public UserInterface()
        {
            InitializeComponent();

            //this.propertyGrid1.SelectedObject = Settings.Instance;

            #if (DEBUG)
                    Console.WriteLine("Mode=Debug");
                    this.WindowState = FormWindowState.Normal;

                    /*new DatabaseConnection();
                    DatabaseConnection.OnError += delegate(Object sender, Exception exception)
                    {
                        this.log("ERROR: Source:" + exception.Message + "\n" + exception.Source + "\n" + (exception.InnerException != null ? exception.InnerException.Message : ""), EventLogEntryType.Error);
                    };*/

                    this.logger = new Logger();
                    this.logger.LogEvent += delegate(String message, EventLogEntryType type)
                    {
                        if (!this.IsDisposed)
                            this.log(message, type);
                    };

                    string key = @"SOFTWARE\IT Global Services\Job Server\Jobs\";

                    RegistryKey jobs = Registry.LocalMachine.OpenSubKey(key);

                    foreach (string v in jobs.GetSubKeyNames())
                    {
                        try
                        {
                            string assemblyLocation = (string)Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\IT Global Services\Job Server\Jobs\" + v, "AssemblyLocation", @"");

                            Assembly assembly = Assembly.LoadFrom(assemblyLocation);

                            foreach (Type item in assembly.GetTypes())
                            {
                                if (!item.IsClass)
                                    continue;

                                if (item.GetInterfaces().Contains(typeof(IJobItem)))
                                {
                                    this.log("IJobItem found: " + item.Assembly.FullName);

                                    IJobItem job = (IJobItem)Activator.CreateInstance(item);
                                    //IJobItem job = (IJobItem) new PushNotificationSender.PushNotificationSender();
                                    job.logger = logger;

                                    JobControl jobControl = new JobControl(job);
                                    jobControl.logger = logger;

                                    this.jobControl.Add(jobControl);
                                }
                            }
                        }
                        catch(Exception exception)
                        {
                            this.log("Assembly couldn't be loaded :" + exception.Message);
                        }
                    }
		                  

            #else
                Console.WriteLine("Mode=Release");
                this.WindowState = FormWindowState.Normal;
                //this.WindowState = FormWindowState.Minimized;
                //this.Hide();

                ServiceController[] scServices;
                scServices = ServiceController.GetServices();

                try
                {
                    this.sc = new ServiceController("ITGS Job Service");
                    this.sc.MachineName = ".";
                    this.sc.ServiceName = "ITGS Job Service";
                    Console.WriteLine("Status = " + this.sc.Status);
                    Console.WriteLine("Can Pause and Continue = " + this.sc.CanPauseAndContinue);
                    Console.WriteLine("Can ShutDown = " + this.sc.CanShutdown);
                    Console.WriteLine("Can Stop = " + this.sc.CanStop);
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message);
                }

                //this.refreshStatus();
                this.timer_Tick(this, null);

                if (this.sc == null)
                    MessageBox.Show("None service found");

                this.timer.Enabled = true;
            #endif
        }

        private void buttonStartStop_Click(object sender, EventArgs e)
        {
        #if (DEBUG)
            this.RunningState = !this.RunningState;

            if (this.RunningState)
                start();
            else
                stop();
        #else
            try
            {
                this.Action.Invoke();
                this.refreshStatus();
            }
            catch(Exception exception)
            {
                MessageBox.Show(exception.Message + "\n" + "Did you start this management console as admin?");
            }
            
        #endif
        }

        private void notifyIcon1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
                this.toggleWindowState();
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            this.toggleWindowState();
        }

        private void toggleWindowState()
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                this.WindowState = FormWindowState.Normal;
                this.contextMenuStrip1.Items["menuItemToggleWindow"].Text = "Hide UI";
            }
            else
            {
                this.WindowState = FormWindowState.Minimized;
                this.contextMenuStrip1.Items["menuItemToggleWindow"].Text = "Open UI";
            }
        }

        private void log(String message, EventLogEntryType type = EventLogEntryType.Information)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate() { this.log(message, type); }));
                return;
            }

            if (!this.IsDisposed)
            {
                this.dataGridView1.Rows.Add(new object[] { "", type, DateTime.Now, message });
                int c = type.ToString().Length;
                File.AppendAllText("log.txt", type.ToString() + ":\t" + (c < 7 ? "\t " : " ") + DateTime.Now + " - " + message + Environment.NewLine);
            }
        }

         private void propertyGrid1_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
            //Properties.Settings.Default.Save();
            
            #if (DEBUG)
                //this.jobControl.Interval = Settings.Instance.Interval * 1000;
            #endif
        }

        #if (DEBUG)
        private void start()
        {
            this.buttonState.Text = "Stop";
            
            foreach(JobControl jobControl in this.jobControl)
                jobControl.start();
            
            this.labelState.Text = "Running...";
        }

        private void stop()
        {
            this.buttonState.Text = "Start";

            foreach (JobControl jobControl in this.jobControl)
                jobControl.stop();

            this.labelState.Text = "stopped";
        }

        ~UserInterface()
        {
            RunningState = false;
            this.notifyIcon1.Visible = false;
        }
         #else
        
        private void refreshStatus()
        {
            try
            {
                this.sc.Refresh();
                this.labelState.Text = this.sc.Status.ToString();

                if (this.sc.Status == ServiceControllerStatus.Running)
                {
                    this.buttonState.Text = "Stop";
                    this.buttonState.Enabled = true;
                    this.Action = sc.Stop;
                }
                if (this.sc.Status == ServiceControllerStatus.Stopped)
                {
                    this.buttonState.Text = "Start";
                    this.buttonState.Enabled = true;
                    this.Action = sc.Start;
                }
                if (this.sc.Status == ServiceControllerStatus.Paused)
                {
                    this.buttonState.Text = "Continue";
                    this.buttonState.Enabled = true;
                    this.Action = sc.Continue;
                }
                if (this.sc.Status == ServiceControllerStatus.StopPending)
                    this.buttonState.Enabled = false;
                if (this.sc.Status == ServiceControllerStatus.StartPending)
                    this.buttonState.Enabled = false;
            }
            catch (Exception exception)
            {
                this.log(exception.Message, EventLogEntryType.Error);
            }
        }
        #endif

        private void timer_Tick(object sender, EventArgs e)
        {
            #if (DEBUG)
            #else
            this.refreshStatus();

            String logName = "ITGS " + this.serviceName + " Log";

            System.Diagnostics.EventLog eventLog1 = new System.Diagnostics.EventLog();

            eventLog1.Source = this.serviceName;
            eventLog1.Log = logName;
            
            if (!System.Diagnostics.EventLog.SourceExists(serviceName))
                System.Diagnostics.EventLog.CreateEventSource(serviceName, logName);
                                    
            int highestLogIndex = 0;
            
            foreach (System.Diagnostics.EventLogEntry entry in eventLog1.Entries)
            {
                if (entry.Index > this.lastLogIndex)
                {
                    this.dataGridView1.Rows.Add(new object[] { entry.Index, entry.EntryType, entry.TimeGenerated, entry.Message });
                    if (entry.Index > highestLogIndex)
                        highestLogIndex = entry.Index;
                }
            }

            if (highestLogIndex > this.lastLogIndex)
                this.lastLogIndex = highestLogIndex;
            
            #endif
        }
                  
        private void UserInterface_FormClosed(object sender, FormClosedEventArgs e)
        {
            #if (DEBUG)
            RunningState = false;
            #endif
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void aboutToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            new About().ShowDialog();
        }

        private void UserInterface_Load(object sender, EventArgs e)
        {
            #if (DEBUG)
            this.start();
            #endif
        }
    }
}

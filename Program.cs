﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Threading;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.ServiceProcess;
using System.Text.RegularExpressions;

namespace ITGSJobServer
{
    static class Program
    {
        /// <summary>
        /// Der Haupteinstiegspunkt für die Anwendung.
        /// </summary>
        /// 
        static Mutex mutex = new Mutex(true, "ITGSJobServer");        

        [STAThread]
        static void Main(string[] args)
        {
            bool config = false;

            foreach (string arg in args)
                if (Regex.Match(arg, "config", RegexOptions.IgnoreCase).Success)
                    config = true;

            if (config)
            {
                //MessageBox.Show(Process.GetCurrentProcess().ProcessName);

                if (mutex.WaitOne(TimeSpan.Zero, true))
                {
                    Application.EnableVisualStyles();
                    Application.SetCompatibleTextRenderingDefault(false);
                    Application.Run(new UserInterface());
                    mutex.ReleaseMutex();
                }
                else
                {
                    MessageBox.Show("Only one instance at a time!\nPlease look at systray");
                }
            }
            else
            {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[] 
                { 
                    new ITGSJobService() 
                };
                ServiceBase.Run(ServicesToRun);
            }            
        }
    }
}

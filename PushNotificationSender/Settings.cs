﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Win32;

namespace PushNotificationSender
{
    public class Settings
    {
        private const String key = @"HKEY_LOCAL_MACHINE\SOFTWARE\IT Global Services\Job Server\Jobs\PushNotificationSender";
        private static Settings settings = new Settings();

        public static Settings Instance { get { return settings; } }

        private Settings()
        {

        }

        public string ConnectionString
        {
            get
            {
                return (string)Registry.GetValue(key, "ConnectionString", @"");
            }

            set
            {
                Registry.SetValue(key, "ConnectionString", value);
            }
        }

        /*public string ConnectionString2
        {
            get
            {
                return (string)Registry.GetValue(key, "ConnectionString2", @"");
            }

            set
            {
                Registry.SetValue(key, "ConnectionString2", value);
            }
        }

        public string ConnectionString3
        {
            get
            {
                return (string)Registry.GetValue(key, "ConnectionString3", @"");
            }

            set
            {
                Registry.SetValue(key, "ConnectionString3", value);
            }
        }

        public string ConnectionString4
        {
            get
            {
                return (string)Registry.GetValue(key, "ConnectionString4", @"");
            }

            set
            {
                Registry.SetValue(key, "ConnectionString4", value);
            }
        }

        public string SMTPHost
        {
            get
            {
                return (string)Registry.GetValue(key, "SMTPHost", @"");
            }

            set
            {
                Registry.SetValue(key, "SMTPHost", value);
            }
        }


        public int SMTPPort
        {
            get
            {
                return (int)Registry.GetValue(key, "SMTPPort", @"");
            }

            set
            {
                Registry.SetValue(key, "SMTPPort", value);
            }
        }

        public string Email
        {
            get
            {
                return (string)Registry.GetValue(key, "Email", @"");
            }

            set
            {
                Registry.SetValue(key, "Email", value);
            }
        }

        public string Fax
        {
            get
            {
                return (string)Registry.GetValue(key, "Fax", @"");
            }

            set
            {
                Registry.SetValue(key, "Fax", value);
            }
        }

        public int Interval
        {
            get
            {
                return (int)Registry.GetValue(key, "Interval", 300);
            }

            set
            {
                Registry.SetValue(key, "Interval", value);
            }
        }

        public string DebugRecipient
        {
            get
            {
                return (string)Registry.GetValue(key, "DebugRecipient", @"");
            }

            set
            {
                Registry.SetValue(key, "DebugRecipient", value);
            }
        }*/
        
        
    }
}

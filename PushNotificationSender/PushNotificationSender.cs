﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Data;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using JobInterface;
using JobInterface.Database;


namespace PushNotificationSender
{
    public class PushNotificationSender : IJobItem
    {
        public Logger logger { get; set; }
        public bool Running { get; private set; }

        enum MODE
        {
            preview,
            pdf
        }
               
        public void start()
        {
            //logger.log("ImageSyncer: start");
            this.Running = true;
            this.processPushNotifications();
        }

        public void stop()
        {
            //logger.log("ImageSyncer: stop");
            this.Running = false;
        }


        public void resume()
        {
            //logger.log("ImageSyncer: resume");
            this.Running = true;
        }

        private void processPushNotifications()
        {
            String[] connectionStrings = new String[] {
                Settings.Instance.ConnectionString               
            };

            foreach (String connectionString in connectionStrings)
            {
                if (!this.Running)
                    break;

                if (connectionString.Length == 0)
                    continue;

                if (connectionString == null || connectionString.Length == 0)
                    continue;

                string queryString = @"SELECT Notification.*, EmployeeOnDevice.DeviceId, DeviceNotificationSubscription.NotificationSubscription, DeviceNotificationSubscription.StatusFlag, EmployeeOnDevice.StatusFlag  FROM Notification
	                                                JOIN EmployeeOnDevice
	                                                ON Notification.LocalEmployeeId = EmployeeOnDevice.LocalEmployeeId
		                                                JOIN DeviceNotificationSubscription
		                                                ON EmployeeOnDevice.DeviceId = DeviceNotificationSubscription.DeviceId
                                                   WHERE (Transmitted IS NULL OR Transmitted = 0)
		                                            AND (EmployeeOnDevice.StatusFlag = 0 OR EmployeeOnDevice.StatusFlag IS NULL)
                                                    AND (Notification.StatusFlag = 0 OR Notification.StatusFlag IS NULL)
                                                    AND (DeviceNotificationSubscription.StatusFlag = 0 OR DeviceNotificationSubscription.StatusFlag IS NULL)
                ";

                try
                {
                    DatabaseConnection dbConnection = new DatabaseConnection(
                        connectionString,
                        delegate(Object sender, Exception exception)
                        {
                            logger.log("ERROR: " + exception.Message + "\n  StackTrace:" + exception.StackTrace + "  Source:" + exception.Source + "\n" + (exception.InnerException != null ? exception.InnerException.Message : ""), EventLogEntryType.Error);
                        }
                    );

                    logger.log("DBConnectionString: " + connectionString + "\n");

                    Query notifications = new Query(dbConnection, queryString);

                    DataTable dataTable = notifications.GetTable();
                    logger.log(dataTable.TableName + ": " + dataTable.Rows.Count + " notifications to be send found");

                    int notificationID;

                    foreach (DataRow row in dataTable.Rows)
                    {
                        notificationID = 0;

                        if (!this.Running)
                            break;

                        try
                        {
                            notificationID = Convert.ToInt32(row["NotificationID"].ToString());
                            int loalEmployeeID = Convert.ToInt32(row["LocalEmployeeID"].ToString());
                            string notificationText = row["NotificationText"].ToString();
                            string deviceID = row["DeviceID"].ToString();
                            string notificationSubscription = row["NotificationSubscription"].ToString();

                            logger.log("NotificationID: " + notificationID + "  LocalEmployeeID: " + loalEmployeeID + "  NotificationText: " + notificationText + "  DeviceID: " + deviceID + "  NotificationSubscription: " + notificationSubscription, EventLogEntryType.Information);

                            string result = this.SendPushTotification(notificationSubscription, notificationText);
                            logger.log("NotificationID: " + notificationID + " sent. Result: " + result);

                            if (!result.Contains("Received"))
                                throw new Exception("Push Notification not received - " + result);
                            //row["StatusFlag"] = 1;
                            //row["NotificationError"] = result;

                            //tableAdapter.Update(new DataRow[] { row });                        

                            Query query = new Query(dbConnection, "UPDATE [dbo].[Notification] SET [NotificationSentDate]=getdate(), [Transmitted] = 1, [Result] = '" + result + "' WHERE [NotificationID]=" + notificationID + ";");
                            query.ExecuteScalar();

                            //row["Quotation_Server"] = base64;

                            //tableAdapter.Update(dataTable);

                        }
                        catch (Exception exception)
                        {
                            logger.log(exception.Message + "\n" + exception.StackTrace, EventLogEntryType.Error);

                            Query query = new Query(dbConnection, "UPDATE [dbo].[Notification] SET [NotificationSentDate]=getdate(), [Transmitted] = 1, [NotificationError] = '" + exception.Message + "' WHERE [NotificationID]=" + notificationID + ";");
                            query.ExecuteScalar();
                            //row["StatusFlag"] = 0;
                            //row["NotificationError"] = "ERROR: " + exception.Message + "\n" + exception.StackTrace;

                            //tableAdapter.Update(new DataRow[] { row });
                        }
                    }
                }
                catch (Exception exception)
                {
                    logger.log("CRITICAL ERROR: " + exception.Message + "\n" + exception.StackTrace + "\n\n" + queryString, EventLogEntryType.Error);
                }

            }
        }

      
        public string SendPushTotification(string subscriptionUri, string notificationText)
        {           
            // Get the URI that the Microsoft Push Notification Service returns to the push client when creating a notification channel.
            // Normally, a web service would listen for URIs coming from the web client and maintain a list of URIs to send
            // notifications out to.

            HttpWebRequest sendNotificationRequest = (HttpWebRequest)WebRequest.Create(subscriptionUri);

            // Create an HTTPWebRequest that posts the toast notification to the Microsoft Push Notification Service.
            // HTTP POST is the only method allowed to send the notification.
            sendNotificationRequest.Method = "POST";

            // The optional custom header X-MessageID uniquely identifies a notification message. 
            // If it is present, the same value is returned in the notification response. It must be a string that contains a UUID.
            // sendNotificationRequest.Headers.Add("X-MessageID", "<UUID>");

            // Create the toast message.

            /*string toastMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
            "<wp:Notification xmlns:wp=\"WPNotification\">" +
            "<wp:Toast>" +
                    "<wp:Text1>" + "Push Notification Test" + "</wp:Text1>" +
                    "<wp:Text2>" + notificationText + "</wp:Text2>" +
                    "<wp:Param>/Page2.xaml?NavigatedFrom=Toast Notification</wp:Param>" +
            "</wp:Toast> " +
            "</wp:Notification>";*/

            // Set the notification payload to send.
            byte[] notificationMessage = Encoding.Default.GetBytes(notificationText);

            // Set the web request content length.
            //sendNotificationRequest.ContentLength = notificationMessage.Length;
            sendNotificationRequest.ContentType = "text/xml";
            sendNotificationRequest.Headers["X-WindowsPhone-Target"] = "toast";
            sendNotificationRequest.Headers["X-NotificationClass"] = "2";


            using (Stream requestStream = sendNotificationRequest.GetRequestStream())
            {
                requestStream.Write(notificationMessage, 0, notificationMessage.Length);
            }

            // Send the notification and get the response.
            HttpWebResponse response = (HttpWebResponse)sendNotificationRequest.GetResponse();
            string notificationStatus = response.Headers["X-NotificationStatus"];
            string notificationChannelStatus = response.Headers["X-SubscriptionStatus"];
            string deviceConnectionStatus = response.Headers["X-DeviceConnectionStatus"];

            // Display the response from the Microsoft Push Notification Service.  
            // Normally, error handling code would be here. In the real world, because data connections are not always available,
            // notifications may need to be throttled back if the device cannot be reached.
            
            return notificationStatus + " | " + deviceConnectionStatus + " | " + notificationChannelStatus;
        }
        
        private string httpRedirect(string url)
        {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
            webRequest.AllowAutoRedirect = false;  // IMPORTANT

            webRequest.Timeout = 10000;           // timeout 10s
            webRequest.Method = "HEAD";

            HttpWebResponse webResponse;
            using (webResponse = (HttpWebResponse)webRequest.GetResponse())
            {
                if ((int)webResponse.StatusCode >= 300 && (int)webResponse.StatusCode <= 399)
                {
                    string uriString = webResponse.Headers["Location"];

                    logger.log("Redirect to " + uriString ?? "NULL");
                    webResponse.Close(); // don't forget to close it - or bad things happen!

                    return uriString;
                }
                else
                    return url;
            }
        }
       
    }
}

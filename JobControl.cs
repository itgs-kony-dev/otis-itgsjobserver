﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Net;
using JobInterface;

namespace ITGSJobServer
{
    class JobControl
    {               
        //public delegate void JobHanlder();

        public bool Running {private set; get; }

        private IJobItem jobObject;
        private System.Timers.Timer timer;

        public Logger logger { private get; set; }

        public double Interval
        {
            set { this.timer.Interval = value; }
            get { return this.timer.Interval;  }
        }

        public JobControl(IJobItem job, double interval = 60000)
        {
            this.jobObject = job;

            this.timer = new System.Timers.Timer();
            //this.timer.Enabled = true;
            this.timer.Interval = interval;
            this.timer.Elapsed += new System.Timers.ElapsedEventHandler(this.OnTimer);
            //this.timer.Start();
        }

        public void stop()
        {
            this.Running = false;
            this.timer.Enabled = false;
            this.jobObject.stop();
            logger.log("Stopped!");
        }

        public String start()
        {
            if (this.Running)
                this.stop();            
           
            this.Running = true;
            logger.log("Starting:");
                       
            //AsyncMethodCaller caller = new AsyncMethodCaller(ad.TestMethod);

            //Task.Run(() => { 
               // while (this.Running)
               // {
            AsyncMethodCaller caller = new AsyncMethodCaller(this.jobObject.start);
            IAsyncResult result = caller.BeginInvoke(delegate(IAsyncResult ar)
            {
                logger.log("Job finished");
            }, null);

                    //this.jobObject.start();

               //     if (this.Running)
               //         System.Threading.Thread.Sleep(500);  
                    this.Running = false;;
               // }               
            //});
            
            this.timer.Enabled = true;

            return this.Running ? "Running" : "stopped";
        }

        public void resume()
        {
            this.Running = false;
            this.jobObject.resume();
            this.timer.Enabled = true;
            logger.log("Resumed");
        }

        public void OnTimer(object sender, System.Timers.ElapsedEventArgs args)
        {
            //logger.log("Tick\n", EventLogEntryType.Information);

            if (!this.Running)
                this.start();
        }

        public delegate void AsyncMethodCaller();

       
    }
}

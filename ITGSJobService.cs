﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Runtime.InteropServices;
using System.Reflection;
using Microsoft.Win32;
using JobInterface;
using JobInterface.Database;

namespace ITGSJobServer
{
    public enum ServiceState
    {
        SERVICE_STOPPED = 0x00000001,
        SERVICE_START_PENDING = 0x00000002,
        SERVICE_STOP_PENDING = 0x00000003,
        SERVICE_RUNNING = 0x00000004,
        SERVICE_CONTINUE_PENDING = 0x00000005,
        SERVICE_PAUSE_PENDING = 0x00000006,
        SERVICE_PAUSED = 0x00000007,
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct ServiceStatus
    {
        public long dwServiceType;
        public ServiceState dwCurrentState;
        public long dwControlsAccepted;
        public long dwWin32ExitCode;
        public long dwServiceSpecificExitCode;
        public long dwCheckPoint;
        public long dwWaitHint;
    };

    partial class ITGSJobService : ServiceBase
    {
        private System.Diagnostics.EventLog eventLog1;
        private int eventID = 0;
        private JobControl jobControl;

        [DllImport("advapi32.dll", SetLastError = true)]
        private static extern bool SetServiceStatus(IntPtr handle, ref ServiceStatus serviceStatus);

        public ITGSJobService()
        {
            InitializeComponent();

            String serviceName = "ITGS Job Service";
            String logName = "ITGS " + serviceName + " Log";

            this.eventLog1 = new System.Diagnostics.EventLog();

            if (!System.Diagnostics.EventLog.SourceExists(serviceName))
                System.Diagnostics.EventLog.CreateEventSource(serviceName, logName);

            eventLog1.Source = serviceName;
            eventLog1.Log = logName;
            eventLog1.WriteEntry("Initializing service...", EventLogEntryType.SuccessAudit);
            
            Logger logger = new Logger();
            logger.LogEvent += delegate(String message, EventLogEntryType type) { eventLog1.WriteEntry(message, type); };

            string key = @"SOFTWARE\IT Global Services\Job Server\Jobs\";

            RegistryKey jobs = Registry.LocalMachine.OpenSubKey(key);

            foreach (string v in jobs.GetSubKeyNames())
            {
                try
                {
                    string assemblyLocation = (string)Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\IT Global Services\Job Server\Jobs\" + v, "AssemblyLocation", @"");

                    Assembly assembly = Assembly.LoadFrom(assemblyLocation);

                    foreach (Type item in assembly.GetTypes())
                    {
                        if (!item.IsClass) continue;
                        if (item.GetInterfaces().Contains(typeof(IJobItem)))
                        {
                            logger.log("IJobItem found: " + item.Assembly.FullName);

                            IJobItem job = (IJobItem)Activator.CreateInstance(item);
                            job.logger = logger;

                            this.jobControl = new JobControl(job);
                            this.jobControl.logger = logger;
                        }
                    }
                }
                catch (Exception exception)
                {
                    logger.log("Assembly not found :" + exception.Message);
                }
            }

            System.Timers.Timer timer = new System.Timers.Timer();
            timer.Interval = 60000;
            timer.Elapsed += new System.Timers.ElapsedEventHandler(this.OnTimer);
            timer.Start();
            
        }

        protected override void OnStart(string[] args)
        {
            ServiceStatus serviceStatus = new ServiceStatus();
            serviceStatus.dwCurrentState = ServiceState.SERVICE_START_PENDING;
            serviceStatus.dwWaitHint = 100000;
            SetServiceStatus(this.ServiceHandle, ref serviceStatus);

            eventLog1.WriteEntry("Service started");
            this.jobControl.start();

            serviceStatus.dwCurrentState = ServiceState.SERVICE_RUNNING;
            SetServiceStatus(this.ServiceHandle, ref serviceStatus);
        }
        
        protected override void OnStop()
        {
            eventLog1.WriteEntry("Service stopped");
            this.jobControl.stop();
        }

        protected override void OnContinue()
        {
            eventLog1.WriteEntry("Service continued");
        }

        public void OnTimer(object sender, System.Timers.ElapsedEventArgs args)
        {
            eventLog1.WriteEntry("Monitoring the System", EventLogEntryType.Information, this.eventID++);
        }   
    }
}
